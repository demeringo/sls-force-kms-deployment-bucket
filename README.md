# sls-force-kms-deployment-bucket

Force serveless framework to use a specific bucket (and optional KMS encryption of bucket content).

## Use case

Serverless framework can encrypt the _content_ of its deployment buckets (the one created automatically).

This is done by tweaking the provider section of serverless.yml.

However, in an AWS environment where S3 bucket *default* encrytion is mandatory (and is enforced by through compliance checks), this is not sufficent as *only* the content will be encrypted, leading to security alerts.

## Usage

- create or note the arn of your KMS key.
- create a bucket outside serverless framework and configure it with a defautl KMS encryption (named `sls-force-kms-deployment-bucket`in this example). Use whatever tool for this (CLI, Web console, Terraform ...)
- use it explicitely in the provider section of serveless.yml.

If you configure your bucket to be encrypted with a specific KMS key _by default_, you do not have to telle serverless framework wich key to use (but you need to be able to access this key as the user deploying).

```yml
deploymentBucket:
    name: sls-force-kms-deployment-bucket
```
